use std::ffi::OsString;
use std::ffi::OsStr;

fn xte<T: AsRef<OsStr>>(arg: T) {
    use std::process::Command;
    Command::new("xte").arg(arg).spawn().unwrap().wait().unwrap();
}

fn xte_keydown<T: AsRef<OsStr>>(arg: T) {
    let r = arg.as_ref();
    let mut s = OsString::with_capacity(8 + r.len());
    s.push("keydown ");
    s.push(r);
    xte(s)
}

fn xte_keyup<T: AsRef<OsStr>>(arg: T) {
    let r = arg.as_ref();
    let mut s = OsString::with_capacity(6 + r.len());
    s.push("keyup ");
    s.push(r);
    xte(s)
}

fn xte_mousedown<T: AsRef<OsStr>>(arg: T) {
    let r = arg.as_ref();
    let mut s = OsString::with_capacity(10 + r.len());
    s.push("mousedown ");
    s.push(r);
    xte(s)
}

fn xte_mouseup<T: AsRef<OsStr>>(arg: T) {
    let r = arg.as_ref();
    let mut s = OsString::with_capacity(8 + r.len());
    s.push("mouseup ");
    s.push(r);
    xte(s)
}

fn xte_mousermove<T: AsRef<OsStr>>(arg: T) {
    let r = arg.as_ref();
    let mut s = OsString::with_capacity(11 + r.len());
    s.push("mousermove ");
    s.push(r);
    xte(s)
}

use std::sync::Mutex;
use std::collections::HashSet;

lazy_static! {
    static ref KEYS: Mutex<HashSet<String>> = Mutex::new(HashSet::new());
}

pub fn keydown(k: &str) {
    let mut s = KEYS.lock().unwrap();
    if !s.contains(k) {
        xte_keydown(k);
        s.insert(k.into());
    }
}
pub fn keyup(k: &str) {
    let mut s = KEYS.lock().unwrap();
    if s.contains(k) {
        xte_keyup(k);
        s.remove(k);
    }
}

pub fn keyset(k: &str, b: bool) {
    match b {
        true  => keydown(k),
        false => keyup(k),
    }
}
