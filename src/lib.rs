extern crate serial;

#[macro_use]
extern crate lazy_static;

extern crate crc32fast;

use crc32fast::Hasher as CRC32;

use std::env;
use std::io;
use std::mem;

use std::io::prelude::*;
use serial::prelude::*;

use std::collections::BTreeSet;

use std::error::Error;

use std::time::Duration;

use std::io::Cursor;

use std::convert::TryInto;

pub mod input;
pub mod smoother;

pub mod dsl;


#[derive(Copy, Clone, Debug)]
pub enum PsxInput {
    L1, R1, L2, R2, L3, R3,
    Left, Right, Up, Down,
    Select, Start,
    X, Y, A, B,
    LeftStickX(u8),
    LeftStickY(u8),
    RightStickX(u8),
    RightStickY(u8),
}

use PsxInput::*;

// Custom Eq/PartialEq/Hash because we don't care about the values
impl PartialEq for PsxInput {
    fn eq(&self, other: &Self) -> bool {
        mem::discriminant(self) == mem::discriminant(other)
    }
}

impl Eq for PsxInput {
}

impl std::hash::Hash for PsxInput {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        mem::discriminant(self).hash(state)
    }
}

impl std::cmp::PartialOrd for PsxInput {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

impl std::cmp::Ord for PsxInput {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        static order: &[PsxInput] = &[
            L1, R1, L2, R2, L3, R3,
            Left, Right, Up, Down,
            Select, Start,
            X, Y, A, B,
            LeftStickX(0),
            LeftStickY(0),
            RightStickX(0),
            RightStickY(0),
        ];
        let self_in = order.iter().enumerate().find(|&(_, thing)| thing == self).map(|(x, _)| x).unwrap();
        let other_in = order.iter().enumerate().find(|&(_, thing)| thing == other).map(|(x, _)| x).unwrap();
        std::cmp::Ord::cmp(&self_in, &other_in)
    }
}

#[derive(Debug)]
pub struct PsxController {
    m: BTreeSet<PsxInput>,
    oldm: Option<BTreeSet<PsxInput>>,
    nframes: usize,
    since: std::time::Instant,
}

fn normalize(range: f32, center: f32, value: f32) -> f32 {
    ((value-center)/(range-center)).max(-1.0)
}

impl PsxController {
    pub fn analog(&self, stick: PsxInput) -> f32 {
        match self.m.get(&stick).map(|x| *x) {
            Some(LeftStickX(v)) | Some(LeftStickY(v)) => {
                normalize(255.0, 128.0, v as f32)
            },
            Some(RightStickX(v)) | Some(RightStickY(v)) => {
                normalize(255.0, 128.0, v as f32)
            },
            _ => if self.button(stick) { 1.0 } else { 0.0 },
        }
    }

    pub fn button(&self, btn: PsxInput) -> bool {
        self.m.contains(&btn)
    }

    pub fn rising(&self, btn: PsxInput) -> bool {
        self.oldm.as_ref().map(|x| x.contains(&btn) < self.m.contains(&btn)).unwrap_or(false)
    }

    pub fn falling(&self, btn: PsxInput) -> bool {
        self.oldm.as_ref().map(|x| x.contains(&btn) > self.m.contains(&btn)).unwrap_or(false)
    }

    pub fn get_fps(&self) -> f64 {
        let frames = self.nframes as f64;
        let timediff = std::time::Instant::now().duration_since(self.since).as_secs_f64();
        frames / timediff
    }
}

#[derive(Copy,Clone,Debug,PartialEq,Eq)]
pub enum PsxErrorKind {
    Serial(serial::ErrorKind),
    NoArg,
}

#[derive(Debug)]
pub struct PsxError {
    kind: PsxErrorKind,
    description: String,
}

impl From<serial::Error> for PsxError {
    fn from(serial_error: serial::Error) -> PsxError {
        PsxError::new(PsxErrorKind::Serial(serial_error.kind()), serial_error.description())
    }
}

impl From<io::Error> for PsxError {
    fn from(io_error: io::Error) -> PsxError {
        let serial_error: serial::Error = io_error.into();
        serial_error.into()
    }
}

impl std::error::Error for PsxError {
    fn description(&self) -> &str {
        &self.description
    }
}

impl std::fmt::Display for PsxError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(&self.description)
    }
}

impl PsxError {
    pub fn new<T: Into<String>>(kind: PsxErrorKind, description: T) -> Self {
        PsxError { kind, description: description.into() }
    }

    pub fn kind(&self) -> PsxErrorKind {
        self.kind
    }
}

// this was originally a regex
//fn parse_line(line: &mut String) -> Result<BTreeSet<PsxInput>, Option<String>> {
//    if !line.starts_with(":!") {
//        return Err(Some(mem::replace(line, String::new())))
//    }
//    let mut m: BTreeSet<PsxInput> = BTreeSet::new();
//    let mut iter = line.drain(..).skip(2).scan(String::new(), |st, c| {
//        if c == '|' || c == '\x0A' || c == '\x0D' {
//            if st.is_empty() {
//                return Some(None)
//            } else {
//                return Some(Some(mem::replace(st, String::new())))
//            }
//        }
//        st.push(c);
//        Some(None)
//    }).flat_map(|x| x);
//    let mut key = iter.next();
//    macro_rules! k ( ($k:expr, $v:expr) => ({
//        key = match key {
//            Some(ref v) if *v == $k => {
//                m.insert($v);
//                iter.next()
//            },
//            k @ _ => k
//        }
//    }) );
//    k!("Select", Select);
//    k!("L3", L3);
//    k!("R3", R3);
//    k!("Start", Start);
//    k!("U", Up);
//    k!("R", Right);
//    k!("D", Down);
//    k!("L", Left);
//    k!("L2", L2);
//    k!("R2", R2);
//    k!("L1", L1);
//    k!("R1", R1);
//    k!("X", X);
//    k!("A", A);
//    k!("B", B);
//    k!("Y", Y);
//    macro_rules! kv ( ($k:expr, $v:expr) => ({
//        key = match key {
//            Some(ref v) if v.starts_with($k) && v[$k.len()..].parse().map($v).is_ok() => {
//                m.insert($v(v[$k.len()..].parse().unwrap()));
//                iter.next()
//            },
//            k @ _ => k
//        }
//    }) );
//    kv!("LX:", LeftStickX);
//    kv!("LY:", LeftStickY);
//    kv!("RX:", RightStickX);
//    kv!("RY:", RightStickY);
//    if key != None {
//        return Err(None)
//    }
//    Ok(m)
//}

fn parse_buf(printbuf: &mut Cursor<Vec<u8>>, buffer: &[u8]) -> (Option<String>, Option<BTreeSet<PsxInput>>) {
    let mut hasher = CRC32::new();
    hasher.update(&buffer[..6]);
    let hash = hasher.finalize();
    if hash != u32::from_le_bytes(buffer[6..10].try_into().unwrap()) {
        // the message was NOT valid.
        // the whole buffer is invalid.
        printbuf.write_all(&buffer).unwrap();
    }
    let printers = printbuf.get_ref().iter().enumerate().find(|(_, &x)| x == b'\n').map(|(pos, _)| pos).map(|pos| {
        let maybestring = printbuf.get_mut().drain(..=pos).collect::<Vec<u8>>();
        let len = printbuf.get_ref().len();
        printbuf.set_position(len as u64);
        String::from_utf8_lossy(&maybestring).into_owned()
    });
    if hash != u32::from_le_bytes(buffer[6..10].try_into().unwrap()) {
        return (printers, None);
    }

    let main_buttons = buffer[0];
    let extra_buttons = buffer[1];
    let lx = buffer[2];
    let ly = buffer[3];
    let rx = buffer[4];
    let ry = buffer[5];

    let mut m: BTreeSet<PsxInput> = BTreeSet::new();

    if main_buttons & 0x80 != 0 { m.insert(Left); }
    if main_buttons & 0x40 != 0 { m.insert(Down); }
    if main_buttons & 0x20 != 0 { m.insert(Right); }
    if main_buttons & 0x10 != 0 { m.insert(Up); }
    if main_buttons & 0x08 != 0 { m.insert(Start); }
    if main_buttons & 0x04 != 0 { m.insert(R3); }
    if main_buttons & 0x02 != 0 { m.insert(L3); }
    if main_buttons & 0x01 != 0 { m.insert(Select); }

    if extra_buttons & 0x80 != 0 { m.insert(Y); }
    if extra_buttons & 0x40 != 0 { m.insert(B); }
    if extra_buttons & 0x20 != 0 { m.insert(A); }
    if extra_buttons & 0x10 != 0 { m.insert(X); }
    if extra_buttons & 0x08 != 0 { m.insert(R1); }
    if extra_buttons & 0x04 != 0 { m.insert(L1); }
    if extra_buttons & 0x02 != 0 { m.insert(R2); }
    if extra_buttons & 0x01 != 0 { m.insert(L2); }

    m.insert(LeftStickX(lx));
    m.insert(LeftStickY(ly));
    m.insert(RightStickX(rx));
    m.insert(RightStickY(ry));

    (printers, Some(m))
}

pub fn handler<T: FnMut(&PsxController)>(mut f: T) -> Result<(), PsxError> {
    let mut port = if let Some(arg) = env::args_os().skip(1).next() {
        serial::open(&arg)?
    } else {
        return Err(PsxError::new(PsxErrorKind::NoArg, "missing serial port argument"))
    };
    port.reconfigure(&|settings| {
        settings.set_char_size(serial::Bits8);
        settings.set_parity(serial::ParityNone);
        settings.set_stop_bits(serial::Stop1);
        settings.set_flow_control(serial::FlowNone);
        settings.set_baud_rate(serial::Baud115200)
    })?;
    // NOTE since we can't disable timeout, just use the biggest available!
    // note that, because reasons, this isn't 68 years, but rather only 24 days.
    // thank windows for that.
    port.set_timeout(Duration::new(0x7FFFFFFF / 1000, 0))?;
    let mut psxc = PsxController { m: Default::default(), oldm: Default::default(), nframes: 0, since: std::time::Instant::now() };
    let mut oldm = None;
    let mut buffer = [0u8; 20];
    let mut printbuffer: Cursor<Vec<u8>> = Default::default();
    loop {
        let _ = port.read_exact(&mut buffer[..10]);
        let mut hasher = CRC32::new();
        hasher.update(&buffer[..6]);
        if hasher.finalize() != u32::from_le_bytes(buffer[6..10].try_into().unwrap()) {
            for it in 1..11 {
                let partial = &mut buffer[it..];
                let _ = port.read_exact(&mut partial[9..10]);
                let mut hasher = CRC32::new();
                hasher.update(&partial[..6]);
                if hasher.finalize() == u32::from_le_bytes(partial[6..10].try_into().unwrap()) {
                    printbuffer.write_all(&buffer[..it]).unwrap();
                    buffer.copy_within(it.., 0);
                    break;
                }
            }
        }
        let (msg, m) = parse_buf(&mut printbuffer, &buffer);
        if let Some(msg) = msg {
            print!("{}", msg);
            std::io::stdout().flush();
        }
        if let Some(m) = m {
            psxc = PsxController { m, oldm, nframes: psxc.nframes+1, ..psxc };
            f(&psxc);
            oldm = Some(psxc.m);
        }
        //line.clear();
    }
}
