pub struct Smoother {
    last: f32,
    output: f32,
}

impl Smoother {
    pub fn new() -> Self {
        Smoother {
            last: 0.0f32,
            output: 0.0f32,
        }
    }

    pub fn update(&mut self, value: f32) {
        let last = std::mem::replace(&mut self.last, value);
        if self.output == 0.0f32 {
            self.output = value;
            return;
        }
        if is_glitch(last, value) {
            return;
        }
        self.output = (last+value)/2.0f32;
    }

    pub fn get(&self) -> f32 {
        self.output
    }
}

impl Default for Smoother {
    fn default() -> Self {
        Self::new()
    }
}

fn is_glitch(a: f32, b: f32) -> bool {
    let (a, b) = (a.max(b), a.min(b));
    a/b < 0.0f32
}
