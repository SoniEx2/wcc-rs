extern crate psxc;

use psxc::dsl::*;

fn main() {
    let mut xsm = Smoother::new();
    let mut ysm = Smoother::new();
    handler(|psxc| {
        xsm.update(psxc.analog(LeftStickX));
        ysm.update(psxc.analog(LeftStickY));
        auto("Left", xsm.get() <= -0.5 || psxc.button(Left));
        auto("Right", xsm.get() >= 0.5 || psxc.button(Right));
        auto("Up", ysm.get() <= -0.5 || psxc.button(Up));
        auto("Down", ysm.get() >= 0.5 || psxc.button(Down));
        auto("A", psxc.button(Y));
        auto("W", psxc.button(B));
        auto("S", psxc.button(A));
        auto("Q", psxc.button(L1));
        auto("D", psxc.button(X));
        auto("E", psxc.button(R1));
        auto("Escape", psxc.button(Select));
        auto("Return", psxc.button(Start));
        auto("Shift_L", psxc.button(R3));
        auto("Control_L", psxc.button(L2));
        auto("Tab", psxc.button(R2));
    }).unwrap()
}
