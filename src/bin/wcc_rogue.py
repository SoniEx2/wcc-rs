#!/usr/bin/env python

from wii_classic_controller import auto, handler

@handler()
def f(wcc):
  auto('Left', wcc.analog('LeftStickX') <= -0.5 or wcc.button('Left'))
  auto('Right', wcc.analog('LeftStickX') >= 0.5 or wcc.button('Right'))
  auto('Up', wcc.analog('LeftStickY') >= 0.5 or wcc.button('Up'))
  auto('Down', wcc.analog('LeftStickY') <= -0.5 or wcc.button('Down'))
  auto('A', wcc.button('Y'))
  auto('W', wcc.button('B'))
  auto('S', wcc.button('A'))
  auto('Q', wcc.button('L1'))
  auto('D', wcc.button('X'))
  auto('E', wcc.button('R1'))
  auto('Escape', wcc.button('Home'))
  auto('Return', wcc.button('Start'))
  auto('Shift_L', wcc.button('Select'))
  auto('Control_L', wcc.button('L2'))
  auto('Tab', wcc.button('R2'))
