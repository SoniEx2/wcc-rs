extern crate psxc;

use psxc::dsl::*;

fn main() {
    handler(|psxc| {
        auto("Left", psxc.button(Left) || psxc.button(L1));
        auto("Right", psxc.button(Right) || psxc.button(R1));
        auto("space", psxc.button(A) || psxc.button(L1) || psxc.button(R1));
        auto("Escape", psxc.button(Select));
    }).unwrap();
}
