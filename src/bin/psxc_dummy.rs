extern crate psxc;

use psxc::dsl::*;

fn main() {
    //let mut xsm = Smoother::new();
    //let mut ysm = Smoother::new();
    let mut i = 0;
    handler(|psxc| {
        //println!("{:?}", psxc);
        i += 1;
        if i == 1000 {
            println!("{:?}", psxc.get_fps());
            i = 0;
        }
        //xsm.update(psxc.analog(LeftStickX));
        //ysm.update(psxc.analog(LeftStickY));
        //auto("Left", xsm.get() <= -0.5 || psxc.button(Left));
        //auto("Right", xsm.get() >= 0.5 || psxc.button(Right));
        //auto("Up", ysm.get() <= -0.5 || psxc.button(Up));
        //auto("Down", ysm.get() >= 0.5 || psxc.button(Down));
        //auto("C", psxc.button(Cross));
        //auto("X", psxc.button(Square));
        //auto("Z", psxc.button(L1));
        //auto("F", psxc.button(Circle));
        //auto("Return", psxc.button(Start));
        //auto("R", psxc.button(R3));
        //auto("Tab", psxc.button(Select));
    }).unwrap()
}
