#!/usr/bin/env python

from wii_classic_controller import auto, handler

@handler()
def f(wcc):
  auto('Left', wcc.analog('LeftStickX') <= -0.5 or wcc.button('Left') or wcc.button("L1"))
  auto('Right', wcc.analog('LeftStickX') >= 0.5 or wcc.button('Right') or wcc.button("R1"))
  auto('space', wcc.button('A') or wcc.button("L1") or wcc.button("R1"))
  auto('Escape', wcc.button('Home'))
