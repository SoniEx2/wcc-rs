#!/usr/bin/env python

from wii_classic_controller import auto, mouseset, mousermove, handler
import math

MODE = 'Right', 'Left'
SENSITIVITY=[16]

sense = 0
sneak = False

def tomouse(delta):
  #return round(delta * SENSITIVITY[sense])
  ndelta = abs(delta*1.25)**(1/2)
  return round(math.copysign(ndelta * SENSITIVITY[sense], delta))

@handler()
def f(wcc):
  global sense
  if wcc.rising('Start'):
    #sense = (sense + 1) % len(SENSITIVITY)
    SENSITIVITY[0] = SENSITIVITY[0]+1
    print(SENSITIVITY[0])
  if wcc.rising('Select'):
    SENSITIVITY[0] = SENSITIVITY[0]-1
    print(SENSITIVITY[0])
  global sneak
  if wcc.rising('R2'):
    sneak = not sneak
  auto('A', min(0, wcc.analog(MODE[0] + 'StickX')) <= -0.5)
  auto('D', max(0, wcc.analog(MODE[0] + 'StickX')) >=  0.5)
  auto('W', max(0, wcc.analog(MODE[0] + 'StickY')) >=  0.5)
  auto('S', min(0, wcc.analog(MODE[0] + 'StickY')) <= -0.5)
  mouseset('4', wcc.button('Y'))
  mouseset('5', wcc.button('X'))
  auto('space', wcc.button('L1'))
  mouseset('1', wcc.button('R1'))
  mouseset('3', wcc.button('A'))
  mouseset('2', wcc.button('B'))
  auto('Q', wcc.button('L2'))
  auto('E', wcc.button('Down'))
  auto('Escape', wcc.button('Home'))
  auto('Shift_L', sneak)
  dX = wcc.analog(MODE[1] + 'StickX')
  dY = wcc.analog(MODE[1] + 'StickY')
  #print("{} {}".format(dX, dY))
  mousermove(tomouse(dX), tomouse(-dY))
