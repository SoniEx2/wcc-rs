#!/usr/bin/env python

from wii_classic_controller import auto, handler

@handler()
def f(wcc):
  auto('Left', wcc.analog('LeftStickX') <= -0.5 or wcc.button('Left'))
  auto('Right', wcc.analog('LeftStickX') >= 0.5 or wcc.button('Right'))
  auto('Up', wcc.analog('LeftStickY') >= 0.5 or wcc.button('Up'))
  auto('Down', wcc.analog('LeftStickY') <= -0.5 or wcc.button('Down'))
  auto('Return', wcc.button('Start'))
  auto('Shift_R', wcc.button('Select'))
  #auto('E', wcc.button('A'))
  auto('Space', wcc.button('B'))
  auto('Shift_L', wcc.button('Y'))
  #auto('Q', wcc.button('L1'))
  auto('Q', wcc.button('X'))
  #auto('W', wcc.button('R1'))
  auto('apostrophe', wcc.button('Home'))
  #auto('D', wcc.button('L2'))
  #auto('C', wcc.button('R2'))
  #auto('BackSpace', wcc.analog('RightStickX') <= -0.5)
