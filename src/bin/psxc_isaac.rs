extern crate psxc;

use psxc::dsl::*;

fn main() {
    println!("NOT YET IMPLEMENTED! Might not actually work.");
    handler(|psxc| {
        auto("Left", psxc.analog(LeftStickX) <= -0.5 || psxc.button(Left));
        auto("Right", psxc.analog(LeftStickX) >= 0.5 || psxc.button(Right));
        auto("Up", psxc.analog(LeftStickY) >= 0.5 || psxc.button(Up));
        auto("Down", psxc.analog(LeftStickY) <= -0.5 || psxc.button(Down));
        auto("A", psxc.button(Y));
        auto("W", psxc.button(B));
        auto("S", psxc.button(A));
        auto("Q", psxc.button(L1));
        auto("D", psxc.button(X));
        auto("E", psxc.button(R1));
        auto("Escape", psxc.button(R3));
        auto("Return", psxc.button(Start));
        auto("Shift_L", psxc.button(Select));
        auto("Control_L", psxc.button(L2));
        auto("Tab", psxc.button(R2));
    }).unwrap()
}
