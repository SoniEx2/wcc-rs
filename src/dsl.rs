pub use ::PsxInput::*;
pub use ::handler;
pub use ::input::*;
pub use ::input::keyset as auto;
pub use ::smoother::Smoother;
pub use ::PsxInput::{Select as Minus, Start as Plus};
pub use ::PsxInput::{X as Triangle, Y as Square, A as Circle, B as Cross};
#[allow(non_upper_case_globals)]
pub const LeftStickX: ::PsxInput = ::PsxInput::LeftStickX(0);
#[allow(non_upper_case_globals)]
pub const LeftStickY: ::PsxInput = ::PsxInput::LeftStickY(0);
#[allow(non_upper_case_globals)]
pub const RightStickX: ::PsxInput = ::PsxInput::RightStickX(0);
#[allow(non_upper_case_globals)]
pub const RightStickY: ::PsxInput = ::PsxInput::RightStickY(0);
/* 
  rb"""^
      (?P<L1>LB\|)?
      (?P<R1>RB\|)?
      (?P<L2>ZL\|)?
      (?P<R2>ZR\|)?
      (?P<Left>L\|)?
      (?P<Right>R\|)?
      (?P<Up>U\|)?
      (?P<Down>D\|)?
      (?P<Select>(?P<Minus>Select\|))?
      (?P<Start>(?P<Plus>Start\|))?
      (?P<Home>Home\|)?
      (?P<X>X\|)?
      (?P<Y>Y\|)?
      (?P<A>A\|)?
      (?P<B>B\|)?
      LSX:(?P<LeftStickX>[1-5]?\d|6[0-3])\|
      LSY:(?P<LeftStickY>[1-5]?\d|6[0-3])\|
      RSX:(?P<RightStickX>[1-2]?\d|3[01])\|
      RSY:(?P<RightStickY>[1-2]?\d|3[01])\|
      LBA:(?P<L1Travel>[1-2]?\d|3[01])\|
      RBA:(?P<R1Travel>[1-2]?\d|3[01])
      \r\n$""")
      */
